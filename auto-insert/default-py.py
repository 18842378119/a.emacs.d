"""
======================================================================
`(upcase (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))` --- $1

    Author: Zi Liang <frostliang@lilith.com>
    Copyright © `(format-time-string "%Y")`, lilith, all rights reserved.
    Created: `(format-time-string "%e %B %Y")`

    Author: Zi Liang <liangzid@stu.xjtu.edu.cn>
    Copyright © `(format-time-string "%Y")`, ZiLiang, all rights reserved.
    Created: `(format-time-string "%e %B %Y")`
======================================================================
"""


# ------------------------ Code --------------------------------------

## normal import 
import json
from typing import List,Tuple,Dict
import random
from pprint import pprint as ppp
# import pickle
# import os
# from os.path import join, exists
# from collections import Counter,OrderedDict
# from bisect import bisect
# from copy import deepcopy
# import pickle

# import sys
# # sys.path.append("./")
# from tqdm import tqdm

# import numpy as np

# import argparse
# import logging


## deep learning related import
# import numpy as np

# import torch
# from torch.utils.tensorboard import SummaryWriter
# from torch.nn import DataParallel
# from torch.utils.data import Dataset, DataLoader
# from torch.nn import CrossEntropyLoss
# import torch.nn as nn
# from torch.nn.parallel import DistributedDataParallel
# from torchvision.transforms import Compose as ComposeTransformation
# import tensorboardX

# ## transformers related import
# from transformers import T5Tokenizer,T5ForConditionalGeneration
# from transformers import BertTokenizer
# from transformers import pipeline
# import transformers




## running entry
if __name__=="__main__":
    main()
    print("EVERYTHING DONE.")


