;;; `(upcase (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))` --- $1
;;
;; Author: Zi Liang <liangzid@stu.xjtu.edu.cn>
;; Copyright © `(format-time-string "%Y")`, ZiLiang, all rights reserved.
;; Created: `(format-time-string "%e %B %Y")`
;;
;;; Commentary:
;;
;;  $2
;;
;;; Code:

$0

;;; `(file-name-nondirectory (buffer-file-name))` ends here
